package ru.tsc.tambovtsev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.SortTable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private SortTable sortTableType;

    public ProjectListRequest(@Nullable String token, @Nullable SortTable sortTableType) {
        super(token);
        this.sortTableType = sortTableType;
    }

}
