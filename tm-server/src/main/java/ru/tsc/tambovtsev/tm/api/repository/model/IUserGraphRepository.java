package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.User;

public interface IUserGraphRepository extends IGraphRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    void deleteByLogin(@Nullable String login);

}
