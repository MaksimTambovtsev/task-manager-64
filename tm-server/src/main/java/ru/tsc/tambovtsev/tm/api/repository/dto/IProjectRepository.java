package ru.tsc.tambovtsev.tm.api.repository.dto;

import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;

public interface IProjectRepository extends IOwnerRepository<ProjectDTO> {
}