package ru.tsc.tambovtsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        write(new Project("Project 1"));
        write(new Project("Project 2"));
        write(new Project("Project 3"));
    }

    public void write(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        write(new Project("New Project" + System.currentTimeMillis()));
    }

    public void save(@NotNull final Project project) {
        projects.replace(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
